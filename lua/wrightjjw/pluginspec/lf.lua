return {
	"ptzz/lf.vim",
	dependencies = { "voldikss/vim-floaterm" },
	config = function(_, _)
		vim.g.lf_map_keys = 0
	end,
	cmd = {
		"Lfcd",
		"Lflcd",
		"Lf",
		"LfCurrentFile",
		"LfCurrentDirectory",
		"LfWorkingDirectory",
		"LfNewTab",
		"LfCurrentFileNewTab",
		"LfCurrentDirectoryNewTab",
		"LfWorkingDirectoryNewTab",
		"LfCurrentFileExistingOrNewTab",
		"LfCurrentDirectoryExistingOrNewTab",
		"LfWorkingDirectoryExistingOrNewTab",
	},
	keys = {
		{ "<leader>pl", "<cmd>LfWorkingDirectory<cr>", desc = "Open LF (working dir)" },
	},
}
