local function find()
	require("telescope.builtin").find_files({
		no_ignore = false,
		no_ignore_parent = false,
	})
end
local function find_all()
	require("telescope.builtin").find_files({
		hidden = true,
		no_ignore = true,
		no_ignore_parent = true,
	})
end
return {
	"nvim-telescope/telescope.nvim",
	dependencies = { "nvim-lua/plenary.nvim" },
	opts = {},
	cmd = { "TodoTelescope" },
	keys = {
		{ "<C-p>", find, desc = "Find Files", },
		{ "<leader>ff", find, desc = "Find Files", },
		{ "<leader>fF", find_all, desc = "Find ALL Files", },
		{ "<leader>fg", require("telescope.builtin").live_grep, desc = "Live Grep" },
		{ "<leader>fb", require("telescope.builtin").buffers, desc = "Find Buffer" },
		{ "<leader>fh", require("telescope.builtin").help_tags, desc = "Find Help Tag" },
		{ "<leader>ft", require("telescope.builtin").tags, desc = "Find Tag" },
		{ "<leader>fs", require("telescope.builtin").lsp_document_symbols, desc = "Find Document Symbol" },
		{ "<leader>fS", require("telescope.builtin").lsp_workspace_symbols, desc = "Find Workspace Symbol" },
	},
}
