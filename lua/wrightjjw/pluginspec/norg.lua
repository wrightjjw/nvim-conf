return {
	"nvim-neorg/neorg",
	build = ":Neorg sync-parsers",
	dependencies = {
		"nvim-lua/plenary.nvim",
	},
	config = function()
		require("neorg").setup({
			["core.defaults"] = {},
			["core.concealer"] = {},
			["core.dirman"] = {},
			["core.qol.toc"] = {},
		})
	end,
	ft = "norg",
}
