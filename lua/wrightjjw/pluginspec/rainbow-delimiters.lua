return {
    'HiPhish/rainbow-delimiters.nvim',
    opt = {},
    config = function (_, opts)
        vim.cmd.highlight("RainbowDelimiterRed guifg=#ff5b5b ctermbg=White")
        require('rainbow-delimiters.setup').setup(opts)
    end,
}
