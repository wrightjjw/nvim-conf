return {
    "folke/trouble.nvim",
    opts = {},
    cmd = { 'TroubleToggle', 'TodoTrouble' },
    keys = {
        { '<leader>td', '<cmd>TroubleToggle workspace_diagnostics<cr>', desc = 'Toggle workspace diagnostics' },
        { '<leader>tr', '<cmd>TroubleRefresh<cr>', desc = 'Refresh trouble' },
    }
}
