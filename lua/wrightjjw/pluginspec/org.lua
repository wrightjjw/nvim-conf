return {
	{
		"nvim-orgmode/orgmode",
		config = function()
			-- Load treesitter grammar for org
			-- require("orgmode").setup_ts_grammar()

			-- -- Setup treesitter
			-- require("nvim-treesitter.configs").setup({
			-- 	highlight = {
			-- 		enable = true,
			-- 		additional_vim_regex_highlighting = { "org" },
			-- 	},
			-- 	ensure_installed = { "org" },
			-- })

			-- Setup orgmode
			require("orgmode").setup({
				org_agenda_files = {"~/org/**/*", "~/notes/**/*"},
				org_default_notes_file = "~/org/refile.org",
                calendar_week_start_day = 0,
			})
		end,
		ft = "org",
        keys = {
            "<leader>o"
        }
	},

	{ "akinsho/org-bullets.nvim", config = true, ft = "org" },
}
