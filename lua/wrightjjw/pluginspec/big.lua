return {
	-- lazy itself
	"folke/lazy.nvim",

	"Mofiqul/adwaita.nvim",

	-- cool
	{ "wrightjjw/cool.nvim", dev = false },

	-- dotnet
	"jlcrochet/vim-razor",

	{
		"jmederosalvarado/roslyn.nvim",
		dependencies = {
			"neovim/nvim-lspconfig",
		},
		cond = function()
			return vim.fn.has("nvim-0.10") == 1
		end,
		config = true,
		ft = { "cs", "razor" },
	},

	-- files
	{
		"nvim-tree/nvim-tree.lua",
		dependencies = {
			"nvim-tree/nvim-web-devicons",
		},
		opts = {},
		cmd = "NvimTreeToggle",
	},

	{
		"lukas-reineke/headlines.nvim",
		dependencies = {
			"nvim-treesitter/nvim-treesitter",
		},
		config = function()
			require("headlines").setup({
				markdown = {
					fat_headlines = true,
					fat_headline_lower_string = " ",
				},
			})
		end,
		ft = "md",
	},

	-- lsp
	{
		"VonHeikemen/lsp-zero.nvim",
		branch = "v2.x",
		dependencies = {
			-- LSP Support
			{ "neovim/nvim-lspconfig" }, -- Required
			{ -- Optional
				"williamboman/mason.nvim",
				build = function()
					pcall(vim.cmd, "MasonUpdate")
				end,
			},
			{ "williamboman/mason-lspconfig.nvim" }, -- Optional

			-- Autocompletion
			{ "hrsh7th/nvim-cmp" }, -- Required
			{ "hrsh7th/cmp-nvim-lsp" }, -- Required
			{ "hrsh7th/cmp-path" },
			{ "hrsh7th/cmp-buffer" },
			{ "L3MON4D3/LuaSnip" }, -- Required
			{ "saadparwaiz1/cmp_luasnip" },

			-- Snippets
			{ "rafamadriz/friendly-snippets" },
		},
	},

	-- markdown extensions
	{
		"nfrid/markdown-togglecheck",
		dependencies = { "nfrid/treesitter-utils" },
		ft = { "markdown" },
		config = function()
			vim.keymap.set("n", "<leader>mc", require("markdown-togglecheck").toggle, { desc = "Toggle Checkmark" })
			vim.keymap.set("n", "<leader>mC", require("markdown-togglecheck").toggle_box, { desc = "Toggle Checkbox" })
		end,
	},

	-- neogit
	{
		"NeogitOrg/neogit",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"sindrets/diffview.nvim",
		},
		config = function()
			require("neogit").setup({
				disable_builtin_notifications = true,
			})
		end,
		cmd = "Neogit",
	},
	"airblade/vim-gitgutter",

	-- status
	{
		"nvim-lualine/lualine.nvim",
		config = function()
			vim.opt.showmode = false
			require("lualine").setup({
				theme = "eldritch",
			})
		end,
		dependencies = {
			"nvim-tree/nvim-web-devicons",
		},
	},

	-- plenary
	"nvim-lua/plenary.nvim",

	-- telescope
	{ "nvim-telescope/telescope.nvim", dependencies = { "nvim-lua/plenary.nvim" } },
}
