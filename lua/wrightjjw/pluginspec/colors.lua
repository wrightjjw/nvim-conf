return {
	{ "bluz71/vim-moonfly-colors", lazy = false },

	"rafamadriz/neon",
	"sainnhe/edge",
    {
        'eldritch-theme/eldritch.nvim',
        opts = {
            transparent = true,
        }
    },
}
