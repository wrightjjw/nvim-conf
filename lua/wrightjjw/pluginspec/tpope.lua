return {
	{
		"tpope/vim-abolish",
		cmd = { "Abolish", "Subvert", "S" },
	},
	{ "tpope/vim-surround" },
}
