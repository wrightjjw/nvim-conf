return {
    'startup-nvim/startup.nvim',
    dependencies = {
        'nvim-telescope/telescope.nvim',
        'nvim-lua/plenary.nvim',
    },
    event = 'UIEnter',
    opt = {theme = 'evil'},
}
