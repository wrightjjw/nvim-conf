return {
    "folke/todo-comments.nvim",
    dependencies = {
        "folke/trouble.nvim",
        "nvim-telescope/telescope.nvim"
    },
    lazy = false,
    opts = {},
    cmd = { 'TodoTrouble', 'TodoTelescope' },
    keys = {
        { '<leader>to', '<cmd>TodoTrouble<cr>', desc = 'Show todos' },
        { '<leader>fo', '<cmd>TodoTelescope<cr>', desc = 'Find todos' },
    }
}
