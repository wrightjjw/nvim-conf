return {
	"stevearc/oil.nvim",
	opts = {},
	cmd = { "Oil" },
	keys = {
		{ "<leader>po", "<cmd>Oil<cr>", desc = "Open dir with Oil" },
	},
}
