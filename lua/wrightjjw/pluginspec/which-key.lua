return {
    "folke/which-key.nvim",
    lazy = false,
    config = function()
        require("which-key").register({
            b = { name = "Buffer Ops" },
            c = { name = "Ft Ops" },
            d = { name = "DAP" },
            f = { name = "Find" },
            t = { name = "Trouble" },
        }, { prefix = "<leader>" })
    end,
}

