return {
	{
		"NeogitOrg/neogit",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"sindrets/diffview.nvim",
		},
        opts = {
            disable_builtin_notifications = true,
        },
		cmd = "Neogit",
        keys = {
            { '<leader>g', '<cmd>Neogit<cr>', desc = 'Neogit' }
        }
	},
    {
        "airblade/vim-gitgutter",
        config = function ()
            vim.g.gitgutter_map_keys = 0
        end,
        lazy = false
    },
}

