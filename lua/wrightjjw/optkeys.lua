local M = {}

-- LSP
function M.keybind_angular_lsp()
	vim.keymap.set("n", "<leader>cla", function()
		require("lspconfig").angularls.setup({})
	end, { buffer = true, desc = "Start Angular LS" })
end

-- Flutter
function M.keybind_flutter()
	vim.api.nvim_buf_set_keymap(0, "n", "<leader>cs")
	vim.api.nvim_buf_set_keymap(0, "n", "<leader>cs", ":FlutterRun", { desc = "Start Flutter" })
	vim.api.nvim_buf_set_keymap(0, "n", "<leader>cq", ":FlutterQuit")
	vim.api.nvim_buf_set_keymap(0, "n", "<leader>cl", ":FlutterReload")
	vim.api.nvim_buf_set_keymap(0, "n", "<leader>cr", ":FlutterRestart")
end

return M
