local M = {
    EnableTextMode = function()
        vim.o.spell = true
        vim.wo.linebreak = true
    end
}

return M
