vim.g.edge_style = "aura"
vim.g.edge_enable_italic = 1
vim.g.edge_transparent_background = 2
vim.g.edge_dim_inactive_window = 1
vim.cmd.colorscheme('edge')
