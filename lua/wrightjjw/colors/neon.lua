vim.g.neon_style = "doom"
vim.g.neon_bold = true
vim.g.neon_italic_keyword = true
vim.g.neon_transparent = true

vim.cmd.colorscheme("neon")
