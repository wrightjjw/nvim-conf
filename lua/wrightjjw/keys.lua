vim.keymap.set({'n', 'i'}, '<c-h>', '<cmd>noh<cr>', { desc = 'Clear search highlight' })

-- Nvim Tree
vim.keymap.set({'n', 'i'}, '<F2>', '<cmd>NvimTreeToggle<cr>', { desc = 'Toggle File Tree' })

-- Buffers
vim.keymap.set('n', '<leader>bd', '<cmd>bdelete<cr>', { desc = 'Delete Buffer' })
vim.keymap.set('n', '<leader>bD', '<cmd>bdelete!<cr>', { desc = 'Delete Buffer!' })
vim.keymap.set('n', '<leader>bn', '<cmd>bnext<cr>', { desc = 'Next Buffer' })
vim.keymap.set('n', '<leader>bp', '<cmd>bprev<cr>', { desc = 'Prev Buffer' })

-- Quickfix
local function quickfixBuild()
    vim.cmd.make()
    vim.cmd.cw()
end

vim.api.nvim_set_keymap('n', '<leader>cc', '', {
    callback = quickfixBuild,
    desc = ":make and :cw",
})

vim.api.nvim_set_keymap('n', '<leader>cn', ':cnext<cr>', {
    desc = ":cnext",
    noremap = true,
})

vim.api.nvim_set_keymap('n', '<leader>cp', ':cprev<cr>', {
    desc = ":cprev",
    noremap = true,
})
