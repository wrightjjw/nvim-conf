-- line numbers
vim.opt.number = true
vim.opt.relativenumber = true

-- always yank to clipboard
vim.opt.clipboard:append('unnamedplus')

-- always tab with spaces
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.expandtab = true

-- conceal
vim.opt.conceallevel = 2

-- set leader to space
vim.g.mapleader = ' '

-- disable netrw
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- colors!
vim.opt.termguicolors = true

-- move lines
vim.keymap.set("v", "J", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "K", ":m '<-2<CR>gv=gv")

-- treesitter folds
vim.opt.foldmethod = "expr"
vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
