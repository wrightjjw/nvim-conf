require('wrightjjw.sets')
require('wrightjjw.plugins')
require('wrightjjw.keys')
require('wrightjjw.files')
require('wrightjjw.format')
require('wrightjjw.neovide')

-- terminal mode escape
vim.keymap.set('t', '<Esc>', '<C-\\><C-n>')

