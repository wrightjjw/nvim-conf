local formats = {}
formats["lua"] = "stylua %s"
formats["go"] = "gofmt -w %s"

local prettier_cmd = "prettier -w %s"
formats["javascript"] = prettier_cmd
formats["typescript"] = prettier_cmd
formats["javascriptreact"] = prettier_cmd
formats["typescriptreact"] = prettier_cmd

local function format_file()
	if vim.bo.modified then
		vim.notify("File is modified. Not formatting", vim.log.levels.WARN)
		return
	end

	local ft = vim.bo.filetype
	local cmd = formats[ft]
	if cmd then
		local out = vim.fn.system(string.format(cmd, vim.fn.expand("%")))
		vim.notify(out, vim.log.levels.INFO)
		vim.cmd.e()
	else
		vim.notify(string.format("Filetype '%s' does not have a defined formatter.", ft), vim.log.levels.WARN)
	end
end

vim.api.nvim_create_user_command("FormatFile", format_file, {})
vim.keymap.set("n", "<leader>cf", "<cmd>FormatFile<cr>", { desc = "Format File" })
