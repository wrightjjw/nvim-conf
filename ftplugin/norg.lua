local wk = require('which-key')
wk.register{
    ["<leader>c"] = {
        name = 'Neorg Commands',
        c = { ":Neog toc" },
    },
}
