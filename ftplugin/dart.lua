require"wrightjjw.optkeys".keybind_flutter()
vim.bo.shiftwidth = 2

vim.api.nvim_create_autocmd("BufWritePost", {
    pattern = {"*.dart"},
    command = "silent! !dart format %"
})
