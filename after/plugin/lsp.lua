local lsp = require('lsp-zero').preset({})
local tsb = require('telescope.builtin')
local wk = require('which-key')

lsp.on_attach(function(client, bufnr)
  lsp.default_keymaps({buffer = bufnr})
    wk.register({
        ["<leader>"] = {
            l = {
                name = "LSP",
                a = { vim.lsp.buf.code_action, "Code Actions" },
                d = { vim.diagnostic.open_float, "Show Diagnostic" },
                r = { vim.lsp.buf.rename, "Rename" },
            },
        },
        g = {
            r = { tsb.lsp_references, "Find References" },
            d = { tsb.lsp_definitions, "Find Definitions" },
        },
    })
end)

-- (Optional) Configure lua language server for neovim
require('lspconfig').lua_ls.setup(lsp.nvim_lua_ls())

lsp.setup()

local cmp = require('cmp')
local cmp_action = require('lsp-zero').cmp_action()
cmp.setup{
    sources = {
        { name = 'path' },
        { name = 'nvim_lsp' },
        { name = 'buffer' },
        { name = 'luasnip' },
    },
    mapping = {
        ['<C-f>'] = cmp_action.luasnip_jump_forward(),
        ['<C-b>'] = cmp_action.luasnip_jump_backward(),
    },
}
